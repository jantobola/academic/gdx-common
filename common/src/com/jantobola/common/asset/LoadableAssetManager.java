package com.jantobola.common.asset;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.jantobola.common.screen.loadable.Loadable;

/**
 * LoadableAssetManager
 *
 * @author Jan Tobola, 2015
 */
public class LoadableAssetManager extends AssetManager {

    public void load(Loadable loadable) {
        AssetRequests requests = loadable.getAssetRequests();

        for (AssetDescriptor assetDescriptor : requests.getRequests().values()) {
            load(assetDescriptor);
        }
    }

    public int releaseAssets(Loadable loadable) {
        AssetRequests requests = loadable.getAssetRequests();
        int refReleased = 0;

        for (ReleasableAssetDescriptor assetDescriptor : requests.getRequests().values()) {
            if(assetDescriptor.isReleasable()){
                unload(assetDescriptor.fileName);
                refReleased++;
            }
        }

        return refReleased;
    }

    public <T> T fetchAsset(AssetDescriptor<T> assetDescriptor) {
        return get(assetDescriptor);
    }

}
