package com.jantobola.common.entity.system;

import com.badlogic.ashley.core.Entity;

/**
 * IteratingEntitySystem
 *
 * @author Jan Tobola, 2015
 */
public abstract class IteratingEntitySystem extends EntitySystem {

    public IteratingEntitySystem() {
        super();
    }

    public IteratingEntitySystem(int priority) {
        super(priority);
    }

    public abstract void update(Entity entity, float delta);

    @Override
    public void update(float delta) {
        for(int i = 0; i < entities.size(); i++) {
            update(entities.get(i), delta);
        }
    }

}
