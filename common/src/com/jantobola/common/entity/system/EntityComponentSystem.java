package com.jantobola.common.entity.system;

import com.jantobola.common.entity.EntityEngine;

/**
 * EntityComponentSystem
 *
 * @author Jan Tobola, 2015
 */
public interface EntityComponentSystem {

    void create(EntityEngine entityEngine);

}
