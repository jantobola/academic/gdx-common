package com.jantobola.common.entity;

import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.utils.Array;

/**
 * EntityEngine
 *
 * @author Jan Tobola, 2015
 */
public class EntityEngine extends PooledEngine {

    private Array<EntitySystem> refSystems = new Array<>();

    public EntityEngine() {
        super();
    }

    public EntityEngine(int entityPoolInitialSize, int entityPoolMaxSize, int componentPoolInitialSize, int componentPoolMaxSize) {
        super(entityPoolInitialSize, entityPoolMaxSize, componentPoolInitialSize, componentPoolMaxSize);
    }

    @Override
    public void addSystem(EntitySystem system) {
        super.addSystem(system);
        refSystems.add(system);
    }

    public void removeAllSystems() {
        for(int i = 0; i < refSystems.size; i++) {
            this.removeSystem(refSystems.get(i));
        }
        refSystems.clear();
    }

}
