package com.jantobola.common.app;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.jantobola.common.asset.LoadableAssetManager;
import com.jantobola.common.entity.system.EntityComponentSystem;
import com.jantobola.common.entity.EntityEngine;
import com.jantobola.common.screen.ScreenManager;
import com.jantobola.common.screen.group.AbstractScreenGroup;

/**
 * GameManager
 *
 * @author Jan Tobola, 2015
 */
public abstract class GameManager extends Game {

    private ScreenManager screenManager;
    private EntityEngine entityEngine;
    public LoadableAssetManager assetManager;

    @Override
    public void create() {
        assetManager = new LoadableAssetManager();
        entityEngine = new EntityEngine();
        screenManager = new ScreenManager(this);

        createScreens(screenManager);
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        entityEngine.update(Gdx.graphics.getDeltaTime());
        super.render();
    }

    /** Sets the current screen. {@link Screen#hide()} is called on any old screen, and {@link Screen#show()} is called on the new
     * screen, if any.
     * @param screen may be {@code null}
     */
    public void setScreen (AbstractScreenGroup screen) {

        if (this.screen != null) {
            this.screen.hide();

            if(this.screen instanceof AbstractScreenGroup) {
                if (((AbstractScreenGroup) this.screen).get() instanceof EntityComponentSystem) {
                    entityEngine.removeAllSystems();
                    entityEngine.removeAllEntities();
                }
            }
        }
        this.screen = screen;

        if (this.screen != null) {
            // assets are loaded for a screen in this point
            this.screen.show();

            if(screen.get() instanceof EntityComponentSystem) {
                // create entities and systems for a scene
                ((EntityComponentSystem) screen.get()).create(this.entityEngine);
            }

            this.screen.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        assetManager.dispose();
        screenManager.dispose();

        entityEngine.removeAllEntities();
        entityEngine.clearPools();
    }

    public void quit() {
        Gdx.app.exit();
    }

    public abstract void createScreens(ScreenManager screenManager);

    public ScreenManager getScreenManager() {
        return screenManager;
    }

    public EntityEngine getEntityEngine() {
        return entityEngine;
    }

}
