package com.jantobola.common.screen.group;

import com.badlogic.gdx.assets.AssetManager;
import com.jantobola.common.screen.ScreenManager;
import com.jantobola.common.screen.ScreenType;

/**
 * LoadingScreenGroup represents screen that gives info about progress of loaded resources.
 *
 * @author Jan Tobola, 2015
 */
public class LoadingScreenGroup extends AbstractScreenGroup {

    private AssetManager assetManager;
    private boolean loaded = false;
    private boolean done = false;

    public LoadingScreenGroup(ScreenManager screenManager, AssetManager assetManager) {
        super(screenManager);
        this.assetManager = assetManager;
    }

    @Override
    public void render(float delta) {
        loaded = assetManager.update();
        super.render(delta);

        if(loaded) {
            if(done) {
                currentScreen.nextScreenLoaded();
                currentScreen.goToNextScreen();
            }

            done = true;
        }
    }

    @Override
    public void hide() {
        super.hide();
        loaded = false;
        done = false;
    }

    @Override
    public ScreenType getScreenType() {
        return ScreenType.LOADING;
    }

}
