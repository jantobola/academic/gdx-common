package com.jantobola.common.screen;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;
import com.jantobola.common.app.GameManager;
import com.jantobola.common.screen.group.*;
import com.jantobola.common.screen.loadable.Loadable;
import com.jantobola.common.screen.loadable.LoadableScreen;
import com.kotcrab.vis.ui.VisUI;

/**
 * ScreenManager is responsible for screen switching. It holds multiple screen
 * groups of different types.
 *
 * @author Jan Tobola, 2015
 */
public class ScreenManager implements Disposable {

    private GameManager gameManager;
    private ObjectMap<ScreenType, AbstractScreenGroup> screens = new ObjectMap<>(4);

    private final String MAIN_SCREEN = "__main";
    private String mainLoadingScreen = MAIN_SCREEN;

    public ScreenManager(GameManager gameManager) {
        this(gameManager, true);
    }

    public ScreenManager(GameManager gameManager, boolean createDefaults) {
        this.gameManager = gameManager;
        VisUI.load();

        if(createDefaults) {
            screens.put(ScreenType.MENU, new MenuScreenGroup(this));
            screens.put(ScreenType.GAME, new GameScreenGroup(this));
            screens.put(ScreenType.SPLASH, new SplashScreenGroup(this));
            screens.put(ScreenType.LOADING, new LoadingScreenGroup(this, gameManager.assetManager));
        }
    }

    public void addScreen(ScreenType type, String id, LoadableScreen screen) {
        screens.get(type).addScreen(id, screen);
    }

    public void addScreen(ScreenType type, LoadableScreen screen) {
        screens.get(type).addScreen(MAIN_SCREEN, screen);
    }

    public LoadableScreen getScreen(LoadableScreen.ScreenInfo info) {
        return screens.get(info.type).get(info.id);
    }

    public void setScreen(ScreenType type, String id) {
        AbstractScreenGroup checkGroup = screens.get(type);

        if(checkGroup == null)
            throw new GdxRuntimeException("No screen group with type \"" + type.toString() + "\" exists.");

        AbstractScreenGroup screen = screens.get(type).setScreen(id);
        LoadableScreen loadable = screen.get();

        if(loadable.requiresAssets()) {
            load(loadable);

            if(screens.get(ScreenType.LOADING).get(mainLoadingScreen) != null) {
                showLoadingScreen(mainLoadingScreen, loadable.thisScreen);
                return;
            } else {
                gameManager.assetManager.finishLoading();
            }
        }

        gameManager.setScreen(screen);
    }

    public void setScreen(ScreenType type) {
        setScreen(type, MAIN_SCREEN);
    }

    public void addScreenGroup(AbstractScreenGroup group) {
        AbstractScreenGroup checkGroup = screens.get(group.getScreenType());

        if(checkGroup == null) {
            screens.put(group.getScreenType(), group);
        } else {
            throw new GdxRuntimeException("Screen group with \"" + checkGroup.getScreenType().toString() + "\" type already exists.");
        }
    }

    public void removeScreenGroup(ScreenType type) {
        screens.remove(type);
    }

    public void removeAllScreenGroups() {
        screens.clear();
    }

    public void showLoadingScreen(String id, LoadableScreen.ScreenInfo nextScreen) {
        gameManager.setScreen(screens.get(ScreenType.LOADING).setScreen(id).setNext(nextScreen));
    }

    public void setDefaultScreen() {
        setScreen(ScreenType.MENU, MAIN_SCREEN);
    }

    public void setMainLoadingScreen(String mainLoadingScreen) {
        this.mainLoadingScreen = mainLoadingScreen;
    }

    /*
        Asset manager bridge
    */

    private void load(Loadable loadable) {
        gameManager.assetManager.load(loadable);
    }

    public int releaseAssets(Loadable loadable) {
        return gameManager.assetManager.releaseAssets(loadable);
    }

    public <T> T fetchAsset(AssetDescriptor<T> assetDescriptor) {
        return gameManager.assetManager.fetchAsset(assetDescriptor);
    }

    @Override
    public void dispose() {
        VisUI.dispose();

        for (AbstractScreenGroup abstractScreenGroup : screens.values()) {
            abstractScreenGroup.dispose();
        }
    }

    public GameManager getGameManager() {
        return gameManager;
    }
}
